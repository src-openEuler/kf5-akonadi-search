%undefine __cmake_in_source_build
%global framework akonadi-search

# uncomment to enable bootstrap mode
# global bootstrap 1
%if !0%{?bootstrap}
%global tests 1
%endif

Name:           kf5-%{framework}
Version:        23.08.5
Release:        2
Summary:        The Akonadi Search library and indexing agent
License:        LGPLv2+
URL:            https://invent.kde.org/frameworks/%{framework}
Source0:        http://download.kde.org/%{stable}/release-service/%{version}/src/%{framework}-%{version}.tar.xz

%global majmin %majmin_ver_kf5
%global stable %stable_kf5

BuildRequires:  make
BuildRequires:  extra-cmake-modules
BuildRequires:  kf5-rpm-macros
BuildRequires:  kf5-ki18n-devel >= 5.15
BuildRequires:  kf5-kconfig-devel >= 5.15
BuildRequires:  kf5-kcrash-devel >= 5.15
BuildRequires:  kf5-krunner-devel >= 5.15
BuildRequires:  kf5-kcmutils-devel >= 5.15
%global majmin_ver %(echo %{version} | cut -d. -f1,2)
BuildRequires:  kf5-akonadi-mime-devel >= %{majmin_ver} 
BuildRequires:  kf5-akonadi-server-devel >= %{majmin_ver}
BuildRequires:  kf5-kcontacts-devel >= %{majmin_ver}
BuildRequires:  kf5-kcalendarcore-devel
BuildRequires:  kf5-kmime-devel >= %{majmin_ver}
BuildRequires:  qt5-qtbase-devel
BuildRequires:  xapian-core-devel
%if 0%{?tests}
BuildRequires:  dbus-x11
BuildRequires:  time
BuildRequires:  xorg-x11-server-Xvfb
%endif

%description
%{summary}.

%package        devel
Summary:        Development files for %{name}
Requires:       %{name} = %{version}-%{release}
Requires:       qt5-qtbase-devel
Requires:       kf5-kcoreaddons-devel
Requires:       kf5-akonadi-server-devel
Requires:       kf5-akonadi-mime-devel
Requires:       kf5-kcontacts-devel
Requires:       kf5-kmime-devel
Requires:       kf5-kcalendarcore-devel
%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.


%prep
%autosetup -n %{framework}-%{version} -p1

%build
%{cmake_kf5} \
  -DBUILD_TESTING:BOOL=%{?tests:ON}%{!?tests:OFF}
%cmake_build

%install
%cmake_install

%find_lang %{name} --all-name

%check
%if 0%{?tests}
export CTEST_OUTPUT_ON_FAILURE=1
xvfb-run -a \
dbus-launch --exit-with-session \
time \
make test ARGS="--output-on-failure --timeout 30" -C %{_target_platform} ||:
%endif

%ldconfig_scriptlets

%files -f %{name}.lang
%license LICENSES/*
%{_kf5_bindir}/akonadi_html_to_text
%{_kf5_bindir}/akonadi_indexing_agent
%{_kf5_datadir}/akonadi/agents/akonadiindexingagent.desktop
%{_kf5_datadir}/qlogging-categories5/*%{framework}.*
%{_kf5_libdir}/libKPim5AkonadiSearchCore.so.*
%{_kf5_libdir}/libKPim5AkonadiSearchDebug.so.*
%{_kf5_libdir}/libKPim5AkonadiSearchPIM.so.*
%{_kf5_libdir}/libKPim5AkonadiSearchXapian.so.*
%{_kf5_plugindir}/krunner/kcms/kcm_krunner_pimcontacts.so
%{_kf5_plugindir}/krunner/krunner_pimcontacts.so
%{_kf5_qtplugindir}/pim5/akonadi/

%files devel
%{_includedir}/KPim5/AkonadiSearch/
%{_kf5_libdir}/cmake/KPim5AkonadiSearch/
%{_kf5_libdir}/libKPim5AkonadiSearchCore.so
%{_kf5_libdir}/libKPim5AkonadiSearchDebug.so
%{_kf5_libdir}/libKPim5AkonadiSearchPIM.so
%{_kf5_libdir}/libKPim5AkonadiSearchXapian.so

%changelog
* Thu Nov 21 2024 tangjie02 <tangjie02@kylinsec.com.cn> - 23.08.5-2
- adapt to the new CMake macros to fix build failure

* Mon Mar 18 2024 peijiankang <peijiankang@kylinos.cn> - 23.08.5-1
- update verison to 23.08.5

* Mon Jan 08 2024 peijiankang <peijiankang@kylinos.cn> - 23.08.4-1
- update verison to 23.08.4

* Fri Aug 04 2023 jiangxinyu <jiangxinyu@kylinos.cn> - 23.04.3-1
- Update package to version 23.04.3

* Tue Apr 18 2023 jiangxinyu <jiangxinyu@kylinos.cn> - 22.12.0-1
- Package init
